package pl.edu.agh.services;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataConsumer extends TimerTask {

	private final static Logger logger = LoggerFactory.getLogger(DataConsumer.class);

	@Autowired
	private DataGatherer dataGatherer;

	public String consumeData() {
		logger.info("Consuming started");
		StringBuilder sb = new StringBuilder();
		List<Long> data = getDataGatherer().getData();
		logger.info("Data size: " + data.size());
		for (Long l : data) {
			logger.info("Consuming: " + l);
			sb.append("\n" + l + " is prime? : " + isPrime(l));
			// sb.append("\nfactors: ");
			/*
			 * for (Long factor : primeFactors(l)) { sb.append(factor + ", "); }
			 */
		}
		logger.info("Consuming ended");
		return sb.toString();
	}

	private boolean isPrime(Long d) {
		for (int i = 2; i < d; i++) {
			if (d % i == 0)
				return false;
		}
		return true;
	}

	private List<Long> primeFactors(long number) {
		long n = number;
		List<Long> factors = new ArrayList<Long>();
		for (long i = 2; i <= n; i++) {
			while (n % i == 0) {
				factors.add(i);
				n /= i;
			}
		}
		return factors;
	}

	public DataGatherer getDataGatherer() {
		return dataGatherer;
	}

	public void setDataGatherer(DataGatherer dataGatherer) {
		this.dataGatherer = dataGatherer;
	}

	@Override
	public void run() {
		long start = System.currentTimeMillis();
		logger.info(consumeData());
		logger.info("Time: " + (System.currentTimeMillis() - start));
	}
}

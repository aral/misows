package pl.edu.agh.services;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;

@Component
public class DataSender implements CommandLineRunner {

	@Autowired
	private DataConsumer dataConsumer;

	private final static Logger logger = LoggerFactory.getLogger(DataConsumer.class);
	private List<String> receivers = new LinkedList<String>();
	private ScheduledExecutorService executor = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
	private Random rand = new Random();

	public void sendData() throws IOException, URISyntaxException {
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
		for (String receiver : receivers) {
			ClientHttpRequest req = factory.createRequest(new URI(receiver + getBigRandomLong()), HttpMethod.GET);
			long start = System.currentTimeMillis();
			ClientHttpResponse resp = req.execute();
			logger.info("Response: " + resp.getStatusCode() + ", time: " + (System.currentTimeMillis() - start));
		}
	}

	private long getBigRandomLong() {
		return rand.nextLong();
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("Arguments: ");
		if (args.length > 1) {
			String responsibility = args[0];
			if (responsibility.equals("sender")) {
				int milis = Integer.valueOf(args[1]);
				for (int i = 2; i < args.length; ++i) {
					logger.info("Adding receiver: " + args[i]);
					StringBuilder sb = new StringBuilder("http://");
					sb.append(args[i]);
					sb.append("/");
					receivers.add(sb.toString());
				}
				executor.scheduleAtFixedRate(new Runnable() {

					@Override
					public void run() {
						try {
							sendData();
						} catch (IOException | URISyntaxException e) {
							logger.error(e.getMessage(), e);
						}
					}

				}, 0, milis > 0 ? milis : 1, milis > 0 ? TimeUnit.MILLISECONDS : TimeUnit.NANOSECONDS);
			} else {
				int milis = Integer.valueOf(args[1]);
				executor.scheduleAtFixedRate(dataConsumer, 0, milis > 0 ? milis : 1, milis > 0 ? TimeUnit.MILLISECONDS : TimeUnit.NANOSECONDS);
			}
		} else {
			logger.info("Use: java -jar <jarname> <sender|consumer> <activity_rate_in_ms> [host:port host:port etc. - receivers if sender]");
		}
	}

	public DataConsumer getDataConsumer() {
		return dataConsumer;
	}

	public void setDataConsumer(DataConsumer dataConsumer) {
		this.dataConsumer = dataConsumer;
	}
}

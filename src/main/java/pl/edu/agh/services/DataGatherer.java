package pl.edu.agh.services;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class DataGatherer {
	private List<Long> data = Collections.synchronizedList(new LinkedList<Long>());

	public void addData(Long data) {
		synchronized (data) {
			this.data.add(data);
		}

	}

	public void removeData(Long data) {
		this.data.remove(data);
	}

	public synchronized List<Long> getData() {
		List<Long> copy = new LinkedList<Long>(data);
		data.clear();
		return copy;
	}

	public void setData(List<Long> data) {
		this.data = data;
	}

	public void clearData() {
		synchronized (data) {
			data.clear();
		}
	}

}

package pl.edu.agh.controllers;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.edu.agh.services.DataGatherer;

@Controller
public class DataReceiver {

	private final static Logger logger = LoggerFactory.getLogger(DataReceiver.class);

	@Autowired
	private DataGatherer dataGatherer;

	@PostConstruct
	public void init() {
		logger.debug("I'm created!");
	}

	@RequestMapping("/")
	@ResponseBody
	public String welcome() {
		logger.info("Helloooo");
		return "Hello!";
	}

	@RequestMapping("/{data}")
	@ResponseBody
	public String receiveData(@PathVariable("data") Long data) {
		logger.info("Got data: " + data);
		getDataGatherer().addData(data);
		return "OK";
	}

	public DataGatherer getDataGatherer() {
		return dataGatherer;
	}

	public void setDataGatherer(DataGatherer dataGatherer) {
		this.dataGatherer = dataGatherer;
	}
}
